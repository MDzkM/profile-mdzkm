from django.urls import re_path
from .views import index, form
#url for app
urlpatterns = [
    re_path('', index, name='index'),
    re_path('index.html', index, name='index'),    
    re_path('form.html', form, name='form'),
]
